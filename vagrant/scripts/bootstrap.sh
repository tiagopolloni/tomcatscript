#!/bin/bash

echo "[ TASK $((count=1)) ] SET /ETC/HOSTS"
echo "172.20.0.10 servertomcat.com" >> /etc/hosts 

echo "[ TASK $((++count)) ] INSTALANDO PACOTES NECESSARIOS"
    yum install -y vim net-tools wget zip unzip >/dev/null 2>&1

echo "[ TASK $((++count)) ] DESATIVAR SELINUX"
setenforce 0
sed -i 's:SELINUX=enforcing:SELINUX=disabled:' /etc/sysconfig/selinux 

echo "[ TASK $((++count)) ] Stopping and Disabling firewalld"
systemctl disable firewalld >/dev/null 2>&1
systemctl stop firewalld  >/dev/null 2>&1
