#!/bin/bash

# Primeiramente Instalar o JAVA
echo "[ TASK 5 ] Instalando JAVA"
yum install -y java-1.8.0 >/dev/null 2>&1
echo "--------JAVA Instalado com Sucesso!--------"
# Fazer o download do pacote TOMCAT
echo "[ TASK 6 ] Instalando APACHE-TOMCAT"
cd /tmp
wget http://apache.panu.it/tomcat/tomcat-9/v9.0.30/bin/apache-tomcat-9.0.30.zip >/dev/null 2>&1
unzip apache-tomcat-*.zip >/dev/null 2>&1
rm -rf apache-tomcat-*.zip
mv apache-tomcat-* tomcat9
mv tomcat9 /usr/local/
cd /usr/local
chmod +x /usr/local/tomcat9/bin/*
/usr/local/tomcat9/bin/startup.sh >/dev/null 2>&1
sleep 5
echo "--------APACHE-TOMCAT Instalado com Sucesso!--------"
# Configuranto o TOMCAT
echo "[ TASK 7 ] Configurando TOMCAT"
# Criar usuario e senha para acesso:
cat <<'EOF'>> /usr/local/tomcat9/conf/tomcat-users.xml
  <role rolename="manager-gui"/>
  <role rolename="manager-script"/>
  <role rolename="manager-jmx"/>
  <role rolename="manager-status"/>
  <role rolename="admin-gui"/>
  <role rolename="admin-script"/>
  <user username="admin" password="admin" roles="manager-gui, manager-script, manager-jmx, manager-status, admin-gui, admin-script"/>
EOF
# Apaga linha:
sed -i 's#</tomcat-users>##' /usr/local/tomcat9/conf/tomcat-users.xml
# Adiciona Linha
echo "</tomcat-users>" >> /usr/local/tomcat9/conf/tomcat-users.xml

# Dando acesso as paginas de Gerenciamento
cat <<'EOF' >> /usr/local/tomcat9/conf/Catalina/localhost/manager.xml
<Context privileged="true" antiResourceLocking="false"
        docBase="${catalina.home}/webapps/manager">
    <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$" />
</Context>
EOF

cat <<'EOF' >> /usr/local/tomcat9/conf/Catalina/localhost/host-manager.xml
<Context privileged="true" antiResourceLocking="false"
        docBase="${catalina.home}/webapps/manager">
    <Valve className="org.apache.catalina.valves.RemoteAddrValve" allow="^.*$" />
</Context>
EOF

chown vagrant:vagrant -R /usr/local/tomcat9
echo "--------TOMCAT Configurado com Sucesso!--------"
# Reiniciando o TOMCAT
echo "[ TASK 8 ] Restart TOMCAT"
/usr/local/tomcat9/bin/shutdown.sh >/dev/null 2>&1
/usr/local/tomcat9/bin/startup.sh >/dev/null 2>&1
echo "--------APACHE-TOMCAT Pronto para uso!!!!!!--------"
# Endereco IP
hostname -I | awk '{ print $2}'

# Se pagina bloqueada mudar:
#  /tomcat9/webapps/manager/META-INF/context.xml
#  /tomcat9/webapps/host-manager/META-INF/context.xml